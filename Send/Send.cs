﻿using System;
using RabbitMQ.Client;
using System.Text;
using Persistence.RabbitMQ;

class Send
{
    public static void Main()
    {
        string message = "Hello World! 123456789";

        // var obj = new ABC {Id = 1, Name = "Duy"};

        new RabbitManager()
            .DeclareQueue("hello")
            .RoutingKey("hello")
            .Body(message)
            .Publish();

        Console.WriteLine(" [x] Sent {0}", message);

        Console.WriteLine(" Press [enter] to exit.");
        Console.ReadLine();
    }
}

public class ABC 
{
    public int Id {get; set;}
    public string Name {get; set;}
}