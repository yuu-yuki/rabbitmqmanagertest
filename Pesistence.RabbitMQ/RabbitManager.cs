﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.Extensions.ObjectPool;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Newtonsoft.Json;
using System.Collections.Concurrent;
using System.Threading;

namespace Persistence.RabbitMQ
{
    public class RabbitManager : IDisposable
    {
        #region Variables

        private readonly IModel channel;
        private readonly IConnection connection;
        private readonly string replyQueueName;
        //private readonly EventingBasicConsumer consumer;
        private readonly BlockingCollection<string> respQueue = new BlockingCollection<string>();
        private readonly IBasicProperties props;
        private readonly IBasicProperties properties;

        private byte[] _messageBytes { get; set; }
        private string _routingKey { get; set; }

        #endregion

        #region Queue declare

        private string _exchange { get; set; } = "";
        private string _queue { get; set; } = "";
        private bool _durableQueue { get; set; }
        private bool _exclusiveQueue { get; set; }
        private bool _autoDeleteQueue { get; set; }
        private Dictionary<string, object> _argumentsQueue { get; set; }

	    #endregion

        #region Constructor

        public RabbitManager()
        {
            // => Connect
            var factory = new ConnectionFactory() { HostName = "localhost" };
            connection = factory.CreateConnection();

            channel = connection.CreateModel();
            properties = channel.CreateBasicProperties();
        }

        #endregion

        #region Declare

        public RabbitManager DeclareQueue(string queue, bool durable = false, bool exclusive = false, bool autoDelete = false, Dictionary<string, object> arguments = null)
        {
            _queue = queue;
            _durableQueue = durable;
            _exclusiveQueue = exclusive;
            _autoDeleteQueue = autoDelete;
            _argumentsQueue = arguments;

            channel.QueueDeclare
            (
                queue: _queue,
                durable: _durableQueue,
                exclusive: _exclusiveQueue,
                autoDelete: _autoDeleteQueue,
                arguments: _argumentsQueue
            );

            return this;
        }

        #endregion

        #region Body

        public RabbitManager Body(string message)
        {
            // var body = JsonConvert.SerializeObject(message);
            _messageBytes = Encoding.UTF8.GetBytes(message);
            return this;
        }

        #endregion

        #region Routing

        public RabbitManager RoutingKey(string key)
        {
            _routingKey = key;
            return this;
        }

        #endregion

        #region Property

        public RabbitManager Properties(bool persistent = false)
        {
            properties.Persistent = persistent;

            return this;
        }

        #endregion

        #region Publish

        public void Publish()
        {
            // Publish
            channel.BasicPublish
            (
                exchange: _exchange,
                routingKey: _routingKey,
                basicProperties: properties,
                body: _messageBytes
            );

            if (!channel.IsClosed)
                channel.Close();
        }

        //public string PublishAndAwait()
        //{
        //    return "";
        //}

        #endregion

        #region BasicQos

        public RabbitManager BasicQos(uint prefetchSize = 0, ushort prefetchCount = 0, bool global = false)
        {
            channel.BasicQos(prefetchSize, prefetchCount, global);

            return this;
        }

        #endregion

        #region Receive

        public string Receive(bool autoAck = false)
        {
            var resultResponse = "";

            var consumer = new EventingBasicConsumer(channel);

            consumer.Received += (model, ea) =>
            {
                var body = ea.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);

                resultResponse = message;

                channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
            };

            channel.BasicConsume
            (
                queue: _queue,
                autoAck: autoAck,
                consumer: consumer
            );

            if (!channel.IsClosed)
                channel.Close();

            return resultResponse;
        }

        public void Listener(bool autoAck = false, bool multiple = false)
        {
            Console.WriteLine(" [*] Waiting for messages.");

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (sender, ea) =>
            {
                var body = ea.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                Console.WriteLine(" [x] Received {0}", message);

                int dots = message.Split('.').Length - 1;
                Thread.Sleep(dots * 1000);

                Console.WriteLine(" [x] Done");

                // Note: it is possible to access the channel via
                //       ((EventingBasicConsumer)sender).Model here
                channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: multiple);
            };
            channel.BasicConsume(queue: _queue,
                                 autoAck: autoAck,
                                 consumer: consumer);

            Console.WriteLine(" Press [enter] to exit.");
            Console.ReadLine();
        }

        #endregion
    
        #region Disposable

        public void Dispose()
        {
            connection.Close();
        }

        #endregion
    }
}