﻿using System;
using RabbitMQ.Client;
using System.Text;
using Persistence.RabbitMQ;

namespace NewTask
{
    class NewTask
    {
        public static void Main(string[] args)
        {
            var message = GetMessage(args);

            new RabbitManager()
                .DeclareQueue(queue: "task_queue", durable: true, exclusive: false, autoDelete: false, arguments: null)
                .RoutingKey("task_queue")
                .Properties(persistent: true)
                .Body(message)
                .Publish();
        }

        private static string GetMessage(string[] args)
        {
            return ((args.Length > 0) ? string.Join(" ", args) : "Hello World!");
        }
    }
}
