﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;
using Persistence.RabbitMQ;

class Receive
{
    public static void Main()
    {
        var message = new RabbitManager()
                        .DeclareQueue("hello")
                        .Receive();

        Console.WriteLine(" [x] Received {0}", message);
    }
}

public class ABC 
{
    public int Id {get; set;}
    public string Name {get; set;}
}