﻿using System;
using System.Text;
using System.Threading;

using Persistence.RabbitMQ;

namespace Worker
{
    class Worker
    {
        public static void Main(string[] args)
        {
            new RabbitManager()
                .DeclareQueue(queue: "task_queue", durable: true, exclusive: false, autoDelete: false, arguments: null)
                .BasicQos(prefetchSize: 0, prefetchCount: 1, global: false)
                .Listener(autoAck: false, multiple: false);

        }
    }
}
